const fg = require('fast-glob');
const config = require('./config');
const ensureDir = require('./src/ensureDir');
const compress = require('./src/compress');

const { inDir, outDir } = config;

const start = async () => {
  const files = await fg([
    `${inDir}/*.JPG`,
    `${inDir}/*.jpg`,
    `${inDir}/*.jpeg`,
    `${inDir}/*.CR2`,
    `${inDir}/*.ARW`
  ]);

  await ensureDir(outDir);

  await compress(files, inDir, outDir);
};

start()
  .then(() => {
    console.log('All Done!');
  })
  .catch(err => {
    console.error(err);
  });
